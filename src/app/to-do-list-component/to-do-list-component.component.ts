import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-to-do-list-component',
  templateUrl: './to-do-list-component.component.html',
  styleUrls: ['./to-do-list-component.component.scss']
})
export class ToDoListComponentComponent implements OnInit {

  constructor(private service: ServiceService) { }

  listuser:any = []
  ngOnInit(): void {
    this.listuser = this.service.listtodo;
    console.log(this.listuser);
  }

  getCheck(item){
    this.listuser.splice(item, 1)
  }

}
