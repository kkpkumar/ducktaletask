import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServiceService } from '../services/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-to-do-component',
  templateUrl: './create-to-do-component.component.html',
  styleUrls: ['./create-to-do-component.component.scss']
})
export class CreateToDoComponentComponent implements OnInit {

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(){
    this.formRole();
    this.fromData()
  }

  /** ============ Form Validation ========= */

  myForm: FormGroup;
  task_name: FormControl;
  task_des: FormControl;
  task_rep: FormControl;

  formRole() {
    this.task_name = new FormControl('', [ Validators.required ]);
    this.task_des = new FormControl('', [ Validators.required ]);
    this.task_rep = new FormControl('', [ Validators.required ]);
  }

  /** ============ End Form Validation ========= */

  fromData() {
    this.myForm = new FormGroup({
      task_name: this.task_name,
      task_des: this.task_des,
      task_rep: this.task_rep
    });
  }

  formSubmit() {
    if (this.myForm.valid) {
      this.service.listtodo.push({
        name: this.myForm.value
      })
      console.log(this.myForm.value)
      this.myForm.reset();
    }
    else {
      for (let i in this.myForm.controls) {
        this.myForm.controls[i].markAsTouched();
      }
    }
  }
  go(){
    this.router.navigate(["to-do-list"])
  }
}
